
########################################################################################
#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#


# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=,preferred,auto,auto


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
exec-once = waybar

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us, kz
    kb_model =
    kb_options= 
    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

bindr=ALTSHIFT,Shift_L,exec, $HOME/.config/hypr/scripts/switch_kb_layout.sh

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 2
    gaps_out = 2
    border_size = 1
    col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)
    layout = dwindle
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 8
    blur = yes
    blur_size = 3
    blur_passes = 1
    blur_new_optimizations = on
    dim_inactive = yes
    dim_strength = 0.1

    drop_shadow = no
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

  bezier = overshot, 0.05, 0.9, 0.1, 1.05
  bezier = smoothOut, 0.36, 0, 0.66, -0.56
  bezier = smoothIn, 0.25, 1, 0.5, 1

  animation = windows, 1, 5, overshot, slide
  animation = windowsOut, 1, 4, smoothOut, slide
  animation = windowsMove, 1, 4, default
  animation = border, 1, 10, default
  animation = fade, 1, 10, smoothIn
  animation = fadeDim, 1, 10, smoothIn
  animation = workspaces, 1, 6, default

}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device:epic mouse V1 {
    sensitivity = -0.5
}

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true
    mouse_move_enables_dpms = true
    no_vfr = false
    enable_swallow = true
    swallow_regex = ^($term)$
}    

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more


# $
$mainMod = SUPER
$term = kitty

# Bindings
bind = $mainMod, C, exec, alacritty
bind = $mainMod, RETURN, exec, $term
bind = $mainMod, Q, exec, $HOME/.config/hypr/scripts/close
bind = SUPERSHIFT, Q, exec, $HOME/.config/hypr/scripts/logout.sh 
bind = $mainMod, F, exec, nemo
bind = $mainMod, SPACE, togglefloating, 
bind = $mainMod, A, exec, wofi --show drun
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle
bind = $mainMod, G, exec, lutris
bind = $mainMod, S, exec, steam
bind = $mainMod, D, exec, discord
bind = $mainMod, W, exec, firefox
bind = $mainMod, V, exec, virt-manager #redo later

# 3 Simple rules
windowrule = float, file_progress
windowrule = float, confirm
windowrule = float, dialog
windowrule = float, download
windowrule = float, notification
windowrule = float, error
windowrule = float, splash
windowrule = float, confirmreset
windowrule = float, title:Open File
windowrule = float, title:branchdialog
windowrule = float, Lxappearance
windowrule = float, Rofi
windowrule = animation none,Rofi
windowrule = float, viewnior
windowrule = float, Viewnior
windowrule = float, feh
windowrule = float, pavucontrol-qt
windowrule = float, pavucontrol
windowrule = float, file-roller
windowrule = fullscreen, wlogout
windowrule = float, title:wlogout
windowrule = fullscreen, title:wlogout
windowrule = idleinhibit focus, mpv
windowrule = idleinhibit fullscreen, firefox
windowrule=float,copyq
windowrule=size 220 370,copyq
windowrule=move 1170 52,copyq
windowrule=float,system_monitor
windowrule=size 800 560,system_monitor
windowrule=move 454 52,system_monitor
windowrule=float,qpwgraph
windowrule=float,floatcritty
windowrule=move center,floatcritty
windowrule=float,calendar
windowrule=size 721 483,calendar
windowrule=move 594 52,calendar
windowrule=float,network
windowrule=size 488 399,network
windowrule=move 590 52,network
windowrule= opacity 0.99 0.99,Cider
windowrule= opaque,firefox
windowrule= float, title:^(Firefox — Sharing Indicator)
windowrule = float, title:^(Media viewer)$
windowrule = float, title:^(Volume Control)$
windowrule = float, title:^(Picture-in-Picture)$
windowrule = size 800 600, title:^(Volume Control)$
windowrule = move 75 44%, title:^(Volume Control)$

#screenshot
bind = , Print S, exec, $HOME/.config/hypr/sctipts/screensht full
bind = , Print, exec, $HOME/.config/hypr/scripts/screensht area
# wallpaper
exec-once = swaybg -i $HOME/Pictures/dmitry-grozov-lets-begin-22.jpg
# start
exec-once=dunst
exec-once=/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec-once=$HOME/.config/hypr/scripts/portal
exec-once=wl-paste -t text --watch clipman store --no-persist
exec-once=copyq
exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once=systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once=echo us > /tmp/kb_layout
# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d
#Move window with mainMod shift + arrowkeys
bind = SUPER SHIFT, left, movewindow, l
bind = SUPER SHIFT, right, movewindow, r
bind = SUPER SHIFT, up, movewindow, u
bind = SUPER SHIFT, down, movewindow, d
# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Sound
bind=,XF86AudioRaiseVolume,exec,~/.config/hypr/scripts/volume_ctl.sh up
bind=,XF86AudioLowerVolume,exec,~/.config/hypr/scripts/volume_ctl.sh down 
bind=,XF86AudioMute,exec,~/.config/hypr/scripts/volume_ctl.sh mute 
bind=,XF86MonBrightnessDown,exec,~/.config/hypr/scripts/brightness_ctl.sh down 
bind=,XF86MonBrightnessUp,exec,~/.config/hypr/scripts/brightness_ctl.sh up 
bind=SHIFT,XF86MonBrightnessUp,exec,~/.config/hypr/scripts/brightness_ctl.sh max

bind=SUPER,semicolon,exec,playerctl play-pause
bind=SUPER,bracketleft,exec,playerctl next
bind=SUPER,bracketright,exec,playerctl previous


# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Legacy resize mode
# will switch to a submap called resize
bind=$mainMod,R,submap,resize
submap=resize
binde=,right,resizeactive,10 0
binde=,left,resizeactive,-10 0
binde=,up,resizeactive,0 -10
binde=,down,resizeactive,0 10
bind=,escape,submap,reset
bind=,return,submap,reset
bind=,$mainMod R,submap,reset 
submap=reset

##########################################################
